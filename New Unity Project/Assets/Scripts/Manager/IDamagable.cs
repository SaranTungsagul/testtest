﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Manager  //todo
{
    public interface IDamagable
    {
        event Action OnBreaked;
        void TakeHit();
        void Explode();
    }
}

