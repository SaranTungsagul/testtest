﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Manager //todo
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField] private SoundClip[] soundClips;
        [SerializeField] private AudioSource audioSource;

        public static SoundManager Instance { get; private set; }

        public enum Sound
        {
            BGM,
            GotPoint,
            MissedPoint,
            EndSound
            
        }

        [Serializable]
        public struct SoundClip
        {
            public Sound sound;
            public AudioClip audioClip;
            [Range(0, 1)] public float soundVolume;
        }

        public void Awake()
        {

            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(this);

        }

        public void Play(AudioSource audioSource, Sound sound)
        {
            var soundClip = GetSoundClip(sound);
            audioSource.clip = soundClip.audioClip;
            audioSource.volume = soundClip.soundVolume;
            audioSource.Play();

        }

        public void PlayBGM()
        {
            audioSource.loop = true;
            Play(audioSource, Sound.BGM);
        }

        private SoundClip GetSoundClip(Sound sound)
        {
            foreach (var soundClip in soundClips)
            {
                if (soundClip.sound == sound)
                {
                    return soundClip;
                }

            }
            return default(SoundClip);
        }
    }

}

