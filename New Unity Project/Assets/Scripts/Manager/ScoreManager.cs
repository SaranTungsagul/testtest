﻿using TMPro;
using UnityEngine;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI FinalScoreText;

        public static ScoreManager Instance { get; private set; }

        private int playerScore;
        public void Init(GameManager gameManager)
        {
            SetScore(0);
        }
        public void SetScore(int score) //todo : done but cannot set to zero
        {
            playerScore += score;
            scoreText.text = $"Score : {playerScore}";

        }

        private void Awake()
        {
            Debug.Assert(scoreText != null, "scoreText cannot null");

            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(this);
        }

    }
}

