﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;
using Enemy;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {

        [SerializeField] private Button startButton;
        [SerializeField] private Button reStartButton;
        [SerializeField] private RandomSpawn randomspawn;
        [SerializeField] private RectTransform dialog;

        public static GameManager Instance { get; private set; }


        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(this);
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            

            startButton.onClick.AddListener(OnStartButtonClicked);

            //startButton.onClick.AddListener(OnStartButtonClicked);


        }

        void Start()
        {
            
        }

        void Update()
        {

        }
        private void StartGame()
        {
            ScoreManager.Instance.Init(this);
            SpawnRandomSpawn();
            //todo SPawn everythings
        }

        private void Lost()
        {
          
        }

        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            StartGame();
            
        }
        private void SpawnRandomSpawn()
        {
            var RandomSpawn = Instantiate(randomspawn);
        }
    }

}

