﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;
using Manager;

namespace Enemy
{
    public class Rock : MonoBehaviour
    {

        void Update()
        {

        }

        //Done : When rock hit player, rock destroy | When rock hit ground, rock destroy
        private void OnTriggerEnter(Collider other) 
        {

            if (other.CompareTag("Player"))
            {
                ScoreManager.Instance.SetScore(1);
                Destroy(this.gameObject);

                return;
            }

            else if (other.CompareTag("DestroyMissed"))
            {
              
                ScoreManager.Instance.SetScore(-1);
                Destroy(this.gameObject);
                return;
            }

        }
    }
}

